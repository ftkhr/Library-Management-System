﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library_Management_System;

namespace Library_Management_System.LMS.AdminForms
{
    public partial class FormAdmin : Form
    {
        private Form WM { get; set; }
        public FormAdmin(Form WM)
        {
            InitializeComponent();
            this.WM = WM;
            ucApproval1.BringToFront();
            ucApproval1.PopulateGridViewBorrow();
            ucApproval1.PopulateGridViewReturn();
        }

        private void Logout()
        {
            WM.Visible = true;
            this.Visible=false;
        }

        private void btnBook_Click(object sender, EventArgs e)
        {
            pnlSelect.Height = btnBook.Height;
            pnlSelect.Top = btnBook.Top;
            ucBook1.BringToFront();
        }

        private void btnMembers_Click(object sender, EventArgs e)
        {
            pnlSelect.Height = btnMembers.Height;
            pnlSelect.Top = btnMembers.Top;
            ucMembers1.BringToFront();
            ucMembers1.PopulateGridView();
        }

        private void btnAdminInfo_Click(object sender, EventArgs e)
        {
            pnlSelect.Height = btnAdminInfo.Height;
            pnlSelect.Top = btnAdminInfo.Top;
            ucAdminInfo1.BringToFront();
        }
        
        private void FormAdmin_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Logout();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            this.Logout();
        }

        private void ucApproval1_Load(object sender, EventArgs e)
        {

        }

        private void btnRequests_Click(object sender, EventArgs e)
        {
            pnlSelect.Height = btnRequests.Height;
            pnlSelect.Top = btnRequests.Top;
            ucApproval1.BringToFront();
            ucApproval1.PopulateGridViewBorrow();
            ucApproval1.PopulateGridViewReturn();
        }
    }
}
