﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library_Management_System.LMS.Repository;
using Library_Management_System.LMS.Entities;

namespace Library_Management_System.LMS.AdminForms
{   
    
    public partial class UCAdminInfo : UserControl
    {
        bool showed = false;
        public UCAdminInfo()
        {
            InitializeComponent();
            FillTheInfo(AdminRepo.AdminEntityToClass());
        }
        private Admin UpdateInfo()
        {
            Admin admin= new Admin();
            admin.Adress = this.txtAddress.Text;
            admin.Password = this.txtPassword.Text;
            admin.Phone = this.txtPhone.Text;
            admin.Email = this.txtEmail.Text;
            admin.Name = this.txtName.Text;
            admin.DOB = Convert.ToDateTime(this.dtpDOB.Text);
            return admin;
        }
        private void FillTheInfo(Admin admin)
        {
            this.txtID.Text = admin.ID;
            this.txtAddress.Text = admin.Adress;
            this.txtPassword.Text = admin.Password;
            this.txtPhone.Text = admin.Phone;
            this.txtEmail.Text = admin.Email;
            this.txtName.Text = admin.Name;
            this.dtpDOB.Text = admin.DOB.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            if (this.txtID.Text == "" || this.txtAddress.Text == "" || this.txtPassword.Text == "" || this.txtConfirmPassword.Text == "" || this.txtPhone.Text == "" || this.txtEmail.Text == "" || this.txtName.Text == "" || this.dtpDOB.Text == "")
            {
                MessageBox.Show("Invalid Information");
            }
            else if (this.txtConfirmPassword.Text != this.txtPassword.Text)
            {
                MessageBox.Show("Passwords Didnt Match");
            }
            else if (this.txtPhone.Text.Length != 11 || !int.TryParse(this.txtPhone.Text, out int i))
            {
                MessageBox.Show("Invalid Phone Number");
            }
            
            else
            {
                AdminRepo.UpdateAdminTable(UpdateInfo());
                
            }
        }
        

        private void btnShowPassword_Click(object sender, EventArgs e)
        {
            if (!showed)
            {
                txtPassword.PasswordChar = '\0';
                txtConfirmPassword.PasswordChar = '\0';
                showed = true;
            }
            else
            {
                txtPassword.PasswordChar = '*';
                txtConfirmPassword.PasswordChar = '*';
                showed = false;
            }
        }

        
    }
}
