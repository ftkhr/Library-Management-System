﻿namespace Library_Management_System.LMS.AdminForms
{
    partial class UCApproval
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.DGVReturn = new System.Windows.Forms.DataGridView();
            this.rmemberid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rbookid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rtitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rcopies = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rduration = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rstatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rreturned = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtReturn = new System.Windows.Forms.TextBox();
            this.btnApproveReturn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtBorrow = new System.Windows.Forms.TextBox();
            this.btnApproveBorrow = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.DGVBorrow = new System.Windows.Forms.DataGridView();
            this.memberid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bookid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bookname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.copies = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.duration = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.returned = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.approveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.approveReturnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVReturn)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVBorrow)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(770, 555);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.DGVReturn);
            this.panel3.Controls.Add(this.txtReturn);
            this.panel3.Controls.Add(this.btnApproveReturn);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Location = new System.Drawing.Point(8, 303);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(758, 249);
            this.panel3.TabIndex = 29;
            // 
            // DGVReturn
            // 
            this.DGVReturn.AllowUserToAddRows = false;
            this.DGVReturn.AllowUserToDeleteRows = false;
            this.DGVReturn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVReturn.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rmemberid,
            this.rbookid,
            this.rtitle,
            this.rcopies,
            this.rduration,
            this.rstatus,
            this.rreturned});
            this.DGVReturn.Location = new System.Drawing.Point(-1, 53);
            this.DGVReturn.Name = "DGVReturn";
            this.DGVReturn.ReadOnly = true;
            this.DGVReturn.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVReturn.Size = new System.Drawing.Size(760, 188);
            this.DGVReturn.TabIndex = 31;
            // 
            // rmemberid
            // 
            this.rmemberid.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.rmemberid.DataPropertyName = "memberid";
            this.rmemberid.HeaderText = "Member ID";
            this.rmemberid.Name = "rmemberid";
            this.rmemberid.ReadOnly = true;
            // 
            // rbookid
            // 
            this.rbookid.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.rbookid.DataPropertyName = "bookid";
            this.rbookid.HeaderText = "Book ID";
            this.rbookid.Name = "rbookid";
            this.rbookid.ReadOnly = true;
            // 
            // rtitle
            // 
            this.rtitle.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.rtitle.DataPropertyName = "bookname";
            this.rtitle.HeaderText = "Book Title";
            this.rtitle.Name = "rtitle";
            this.rtitle.ReadOnly = true;
            // 
            // rcopies
            // 
            this.rcopies.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.rcopies.DataPropertyName = "copies";
            this.rcopies.HeaderText = "Copies";
            this.rcopies.Name = "rcopies";
            this.rcopies.ReadOnly = true;
            // 
            // rduration
            // 
            this.rduration.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.rduration.DataPropertyName = "duration";
            this.rduration.HeaderText = "Duration";
            this.rduration.Name = "rduration";
            this.rduration.ReadOnly = true;
            // 
            // rstatus
            // 
            this.rstatus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.rstatus.DataPropertyName = "status";
            this.rstatus.HeaderText = "Status";
            this.rstatus.Name = "rstatus";
            this.rstatus.ReadOnly = true;
            this.rstatus.Visible = false;
            // 
            // rreturned
            // 
            this.rreturned.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.rreturned.DataPropertyName = "returned";
            this.rreturned.HeaderText = "Returned";
            this.rreturned.Name = "rreturned";
            this.rreturned.ReadOnly = true;
            // 
            // txtReturn
            // 
            this.txtReturn.Font = new System.Drawing.Font("Century", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReturn.Location = new System.Drawing.Point(133, 17);
            this.txtReturn.Name = "txtReturn";
            this.txtReturn.Size = new System.Drawing.Size(519, 30);
            this.txtReturn.TabIndex = 30;
            this.txtReturn.TextChanged += new System.EventHandler(this.txtReturn_TextChanged);
            // 
            // btnApproveReturn
            // 
            this.btnApproveReturn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(189)))), ((int)(((byte)(50)))));
            this.btnApproveReturn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApproveReturn.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApproveReturn.ForeColor = System.Drawing.Color.White;
            this.btnApproveReturn.Location = new System.Drawing.Point(665, 7);
            this.btnApproveReturn.Name = "btnApproveReturn";
            this.btnApproveReturn.Size = new System.Drawing.Size(96, 40);
            this.btnApproveReturn.TabIndex = 29;
            this.btnApproveReturn.Text = "Approve";
            this.btnApproveReturn.UseVisualStyleBackColor = false;
            this.btnApproveReturn.Click += new System.EventHandler(this.btnApproveReturn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(-3, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 21);
            this.label3.TabIndex = 28;
            this.label3.Text = "Return Request";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtBorrow);
            this.panel2.Controls.Add(this.btnApproveBorrow);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.DGVBorrow);
            this.panel2.Location = new System.Drawing.Point(8, 48);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(758, 249);
            this.panel2.TabIndex = 28;
            // 
            // txtBorrow
            // 
            this.txtBorrow.Font = new System.Drawing.Font("Century", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBorrow.Location = new System.Drawing.Point(138, 21);
            this.txtBorrow.Name = "txtBorrow";
            this.txtBorrow.Size = new System.Drawing.Size(519, 30);
            this.txtBorrow.TabIndex = 29;
            this.txtBorrow.TextChanged += new System.EventHandler(this.txtBorrow_TextChanged);
            // 
            // btnApproveBorrow
            // 
            this.btnApproveBorrow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(189)))), ((int)(((byte)(50)))));
            this.btnApproveBorrow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApproveBorrow.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApproveBorrow.ForeColor = System.Drawing.Color.White;
            this.btnApproveBorrow.Location = new System.Drawing.Point(663, 11);
            this.btnApproveBorrow.Name = "btnApproveBorrow";
            this.btnApproveBorrow.Size = new System.Drawing.Size(96, 40);
            this.btnApproveBorrow.TabIndex = 28;
            this.btnApproveBorrow.Text = "Approve";
            this.btnApproveBorrow.UseVisualStyleBackColor = false;
            this.btnApproveBorrow.Click += new System.EventHandler(this.btnApproveBorrow_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(-5, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 21);
            this.label2.TabIndex = 27;
            this.label2.Text = "Borrow Requests";
            // 
            // DGVBorrow
            // 
            this.DGVBorrow.AllowUserToAddRows = false;
            this.DGVBorrow.AllowUserToDeleteRows = false;
            this.DGVBorrow.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVBorrow.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.memberid,
            this.bookid,
            this.bookname,
            this.copies,
            this.duration,
            this.status,
            this.returned});
            this.DGVBorrow.ContextMenuStrip = this.contextMenuStrip1;
            this.DGVBorrow.Location = new System.Drawing.Point(-1, 57);
            this.DGVBorrow.Name = "DGVBorrow";
            this.DGVBorrow.ReadOnly = true;
            this.DGVBorrow.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVBorrow.Size = new System.Drawing.Size(760, 188);
            this.DGVBorrow.TabIndex = 26;
            // 
            // memberid
            // 
            this.memberid.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.memberid.DataPropertyName = "memberid";
            this.memberid.HeaderText = "Member ID";
            this.memberid.Name = "memberid";
            this.memberid.ReadOnly = true;
            // 
            // bookid
            // 
            this.bookid.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.bookid.DataPropertyName = "bookid";
            this.bookid.HeaderText = "Book ID";
            this.bookid.Name = "bookid";
            this.bookid.ReadOnly = true;
            // 
            // bookname
            // 
            this.bookname.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.bookname.DataPropertyName = "bookname";
            this.bookname.HeaderText = "Book Title";
            this.bookname.Name = "bookname";
            this.bookname.ReadOnly = true;
            // 
            // copies
            // 
            this.copies.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.copies.DataPropertyName = "copies";
            this.copies.HeaderText = "Copies";
            this.copies.Name = "copies";
            this.copies.ReadOnly = true;
            // 
            // duration
            // 
            this.duration.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.duration.DataPropertyName = "duration";
            this.duration.HeaderText = "Duration";
            this.duration.Name = "duration";
            this.duration.ReadOnly = true;
            // 
            // status
            // 
            this.status.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.status.DataPropertyName = "status";
            this.status.HeaderText = "Status";
            this.status.Name = "status";
            this.status.ReadOnly = true;
            // 
            // returned
            // 
            this.returned.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.returned.DataPropertyName = "returned";
            this.returned.HeaderText = "Returned";
            this.returned.Name = "returned";
            this.returned.ReadOnly = true;
            this.returned.Visible = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.approveToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(181, 48);
            // 
            // approveToolStripMenuItem
            // 
            this.approveToolStripMenuItem.Name = "approveToolStripMenuItem";
            this.approveToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.approveToolStripMenuItem.Text = "approve";
            this.approveToolStripMenuItem.Click += new System.EventHandler(this.approveToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cooper Std Black", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(238, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(314, 45);
            this.label1.TabIndex = 1;
            this.label1.Text = "Book Requests";
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.approveReturnToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(158, 26);
            // 
            // approveReturnToolStripMenuItem
            // 
            this.approveReturnToolStripMenuItem.Name = "approveReturnToolStripMenuItem";
            this.approveReturnToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.approveReturnToolStripMenuItem.Text = "Approve Return";
            this.approveReturnToolStripMenuItem.Click += new System.EventHandler(this.approveReturnToolStripMenuItem_Click);
            // 
            // UCApproval
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Century", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.Name = "UCApproval";
            this.Size = new System.Drawing.Size(776, 561);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVReturn)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGVBorrow)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem approveToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem approveReturnToolStripMenuItem;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView DGVReturn;
        private System.Windows.Forms.DataGridViewTextBoxColumn rmemberid;
        private System.Windows.Forms.DataGridViewTextBoxColumn rbookid;
        private System.Windows.Forms.DataGridViewTextBoxColumn rtitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn rcopies;
        private System.Windows.Forms.DataGridViewTextBoxColumn rduration;
        private System.Windows.Forms.DataGridViewTextBoxColumn rstatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn rreturned;
        private System.Windows.Forms.TextBox txtReturn;
        private System.Windows.Forms.Button btnApproveReturn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtBorrow;
        private System.Windows.Forms.Button btnApproveBorrow;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView DGVBorrow;
        private System.Windows.Forms.DataGridViewTextBoxColumn memberid;
        private System.Windows.Forms.DataGridViewTextBoxColumn bookid;
        private System.Windows.Forms.DataGridViewTextBoxColumn bookname;
        private System.Windows.Forms.DataGridViewTextBoxColumn copies;
        private System.Windows.Forms.DataGridViewTextBoxColumn duration;
        private System.Windows.Forms.DataGridViewTextBoxColumn status;
        private System.Windows.Forms.DataGridViewTextBoxColumn returned;
    }
}
