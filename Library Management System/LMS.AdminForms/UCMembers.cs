﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library_Management_System.LMS.Entities;
using Library_Management_System.LMS.Repository;
using Library_Management_System.LMS.DataLayer;
namespace Library_Management_System.LMS.AdminForms
{
    public delegate void PassMethod();
    public partial class UCMembers : UserControl
    {   
        
        public PassMethod Pass { get; set; }

        public UCMembers()
        {
            InitializeComponent();
            this.PopulateGridView();
        }
        
        public void PopulateGridView()
        {
            this.dgvMembers.AutoGenerateColumns = false;
            this.dgvMembers.DataSource = null;
            this.dgvMembers.DataSource = MemberRepo.GetMembers();
        }
        internal Member FieldToClass()
        {
            string name = this.txtName.Text;
            string address = this.txtAddress.Text;
            DateTime dob = Convert.ToDateTime( this.dtpDob.Text);
            string phone = this.txtPhone.Text;
            string email = this.txtEmail.Text;
            string password = this.txtPassword.Text;
            string accountstatus = "Valid" ;
            int borrowedbook = 0;
            int fine = 0;
            return new Member(name,address, dob, phone , email, password, accountstatus, fine, borrowedbook);
        }
        internal void ClearFields()
        {
            this.txtAddress.Clear();
            this.txtName.Clear();
            this.txtAddress.Clear();
            this.txtPhone.Clear();
            this.txtEmail.Clear();
            this.txtPassword.Clear(); 
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if(this.txtAddress.Text==""||this.txtEmail.Text == "" ||this.txtName.Text == "" ||this.txtPassword.Text == "" ||this.txtPhone.Text == "")
            {
                MessageBox.Show("Invalid Info");
            }
            else if(this.txtPhone.TextLength!=11|| !int.TryParse(this.txtPhone.Text, out int i))
            {
                MessageBox.Show("Invalid Phone Number");
            }
            else
            {
                MemberRepo.InsertMember(this.FieldToClass());
                this.ClearFields();
                this.PopulateGridView();
            }
        }
        public void DeleteCommand()
        {
            MemberRepo.DeleteMember(dgvMembers.CurrentRow.Cells["id"].Value.ToString());
            PopulateGridView();
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            Pass= DeleteCommand;
            ConfirmationPanel cp = new ConfirmationPanel(Pass);
            cp.Show();

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            this.dgvMembers.AutoGenerateColumns = false;
            this.dgvMembers.DataSource = null;
            this.dgvMembers.DataSource = MemberRepo.GetSelectedMember(this.txtSearch.Text);
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Pass = DeleteCommand;
            ConfirmationPanel cp = new ConfirmationPanel(Pass);
            cp.Show();
        }

        private void dgvMembers_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Pass = PopulateGridView;
            UpdateMembers um = new UpdateMembers(Pass, dgvMembers.CurrentRow.Cells["id"].Value.ToString());
            um.Show();
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Pass = PopulateGridView;
            UpdateMembers um = new UpdateMembers(Pass, dgvMembers.CurrentRow.Cells["id"].Value.ToString());
            um.Show();
        }
    }
}
