﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library_Management_System.LMS.AdminForms
{
    public partial class ConfirmationPanel : Form
    {
        PassMethod Pass{ get; set; }
        public ConfirmationPanel(PassMethod pass)
        {
            this.Pass = pass;

            InitializeComponent();
        }
        
        private void btnYes_Click(object sender, EventArgs e)
        {
            this.Pass();
            this.Close();
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
