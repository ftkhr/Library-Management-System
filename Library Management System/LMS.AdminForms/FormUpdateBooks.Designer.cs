﻿namespace Library_Management_System.LMS.AdminForms
{
    partial class FormUpdateBooks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlAddBook = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.dtpPublishingDate = new System.Windows.Forms.DateTimePicker();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblPublishingTime = new System.Windows.Forms.Label();
            this.txtCopies = new System.Windows.Forms.TextBox();
            this.lblCopies = new System.Windows.Forms.Label();
            this.cmbGenre = new System.Windows.Forms.ComboBox();
            this.lblGenre = new System.Windows.Forms.Label();
            this.txtWriter = new System.Windows.Forms.TextBox();
            this.lblWriter = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblbookInfo = new System.Windows.Forms.Label();
            this.pnlAddBook.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlAddBook
            // 
            this.pnlAddBook.Controls.Add(this.btnClose);
            this.pnlAddBook.Controls.Add(this.dtpPublishingDate);
            this.pnlAddBook.Controls.Add(this.btnSave);
            this.pnlAddBook.Controls.Add(this.lblPublishingTime);
            this.pnlAddBook.Controls.Add(this.txtCopies);
            this.pnlAddBook.Controls.Add(this.lblCopies);
            this.pnlAddBook.Controls.Add(this.cmbGenre);
            this.pnlAddBook.Controls.Add(this.lblGenre);
            this.pnlAddBook.Controls.Add(this.txtWriter);
            this.pnlAddBook.Controls.Add(this.lblWriter);
            this.pnlAddBook.Controls.Add(this.txtTitle);
            this.pnlAddBook.Controls.Add(this.lblTitle);
            this.pnlAddBook.Controls.Add(this.lblbookInfo);
            this.pnlAddBook.Location = new System.Drawing.Point(12, 12);
            this.pnlAddBook.Name = "pnlAddBook";
            this.pnlAddBook.Size = new System.Drawing.Size(443, 244);
            this.pnlAddBook.TabIndex = 1;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Century", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(408, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(32, 34);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "×";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dtpPublishingDate
            // 
            this.dtpPublishingDate.Location = new System.Drawing.Point(145, 168);
            this.dtpPublishingDate.Name = "dtpPublishingDate";
            this.dtpPublishingDate.Size = new System.Drawing.Size(267, 20);
            this.dtpPublishingDate.TabIndex = 5;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(168)))), ((int)(((byte)(255)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Century", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.Location = new System.Drawing.Point(177, 200);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(87, 32);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Update";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblPublishingTime
            // 
            this.lblPublishingTime.AutoSize = true;
            this.lblPublishingTime.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPublishingTime.ForeColor = System.Drawing.Color.White;
            this.lblPublishingTime.Location = new System.Drawing.Point(33, 168);
            this.lblPublishingTime.Name = "lblPublishingTime";
            this.lblPublishingTime.Size = new System.Drawing.Size(105, 17);
            this.lblPublishingTime.TabIndex = 10;
            this.lblPublishingTime.Text = "Publishing Time";
            // 
            // txtCopies
            // 
            this.txtCopies.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCopies.Location = new System.Drawing.Point(119, 140);
            this.txtCopies.Name = "txtCopies";
            this.txtCopies.Size = new System.Drawing.Size(293, 22);
            this.txtCopies.TabIndex = 4;
            // 
            // lblCopies
            // 
            this.lblCopies.AutoSize = true;
            this.lblCopies.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCopies.ForeColor = System.Drawing.Color.White;
            this.lblCopies.Location = new System.Drawing.Point(33, 143);
            this.lblCopies.Name = "lblCopies";
            this.lblCopies.Size = new System.Drawing.Size(53, 17);
            this.lblCopies.TabIndex = 7;
            this.lblCopies.Text = "Copies";
            // 
            // cmbGenre
            // 
            this.cmbGenre.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGenre.Font = new System.Drawing.Font("Century", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGenre.FormattingEnabled = true;
            this.cmbGenre.Items.AddRange(new object[] {
            "Horror",
            "Fantasy",
            "Science Fiction",
            "Romance",
            "Thriller",
            "Detective",
            "Children Book"});
            this.cmbGenre.Location = new System.Drawing.Point(119, 110);
            this.cmbGenre.Name = "cmbGenre";
            this.cmbGenre.Size = new System.Drawing.Size(293, 23);
            this.cmbGenre.TabIndex = 3;
            // 
            // lblGenre
            // 
            this.lblGenre.AutoSize = true;
            this.lblGenre.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGenre.ForeColor = System.Drawing.Color.White;
            this.lblGenre.Location = new System.Drawing.Point(33, 113);
            this.lblGenre.Name = "lblGenre";
            this.lblGenre.Size = new System.Drawing.Size(47, 17);
            this.lblGenre.TabIndex = 5;
            this.lblGenre.Text = "Genre";
            // 
            // txtWriter
            // 
            this.txtWriter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWriter.Location = new System.Drawing.Point(119, 82);
            this.txtWriter.Name = "txtWriter";
            this.txtWriter.Size = new System.Drawing.Size(293, 22);
            this.txtWriter.TabIndex = 2;
            // 
            // lblWriter
            // 
            this.lblWriter.AutoSize = true;
            this.lblWriter.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWriter.ForeColor = System.Drawing.Color.White;
            this.lblWriter.Location = new System.Drawing.Point(33, 85);
            this.lblWriter.Name = "lblWriter";
            this.lblWriter.Size = new System.Drawing.Size(45, 17);
            this.lblWriter.TabIndex = 3;
            this.lblWriter.Text = "Writer";
            // 
            // txtTitle
            // 
            this.txtTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitle.Location = new System.Drawing.Point(119, 54);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(293, 22);
            this.txtTitle.TabIndex = 1;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(33, 57);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(68, 17);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Book Title";
            // 
            // lblbookInfo
            // 
            this.lblbookInfo.AutoSize = true;
            this.lblbookInfo.Font = new System.Drawing.Font("Cooper Std Black", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblbookInfo.ForeColor = System.Drawing.Color.White;
            this.lblbookInfo.Location = new System.Drawing.Point(128, 0);
            this.lblbookInfo.Name = "lblbookInfo";
            this.lblbookInfo.Size = new System.Drawing.Size(168, 35);
            this.lblbookInfo.TabIndex = 0;
            this.lblbookInfo.Text = "Book Info";
            // 
            // FormUpdateBooks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.ClientSize = new System.Drawing.Size(465, 268);
            this.Controls.Add(this.pnlAddBook);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormUpdateBooks";
            this.Text = "Update Book";
            this.pnlAddBook.ResumeLayout(false);
            this.pnlAddBook.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlAddBook;
        private System.Windows.Forms.DateTimePicker dtpPublishingDate;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblPublishingTime;
        private System.Windows.Forms.TextBox txtCopies;
        private System.Windows.Forms.Label lblCopies;
        private System.Windows.Forms.ComboBox cmbGenre;
        private System.Windows.Forms.Label lblGenre;
        private System.Windows.Forms.TextBox txtWriter;
        private System.Windows.Forms.Label lblWriter;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblbookInfo;
        private System.Windows.Forms.Button btnClose;
    }
}