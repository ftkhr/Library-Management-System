﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library_Management_System.LMS.Repository;
using Library_Management_System.LMS.Entities;


namespace Library_Management_System.LMS.AdminForms
{
    public partial class FormUpdateBooks : Form
    {
        PassMethod Pass { get; set; }
        internal string ID { get; set; }
        public FormUpdateBooks(string id,PassMethod pass)
        {
            InitializeComponent();
            this.Pass= pass;
            this.ID = id;
            this.FillInfo(GetBook());
        }
        public bool CheckFields()
        {
            if (txtCopies.Text == "" || txtTitle.Text == "" || txtWriter.Text == "" || cmbGenre.Text == "")
            {
                MessageBox.Show("Invalid Info");
                return false;
            }
            else if (!int.TryParse(txtCopies.Text.ToString(), out int i))
            {
                MessageBox.Show("Invalid Info");
                return false;
            }
            else return true;
        }
        internal void FillInfo(Book book)
        {
            this.txtTitle.Text = book.Title;
            this.txtCopies.Text = book.Copies.ToString();
            this.txtWriter.Text = book.Writer;
            this.cmbGenre.Text = book.Genre;
            this.dtpPublishingDate.Text = book.PublishingDate.ToString();
        }
        
        internal Book FieldToClass()
        {
            string id = ID;
            string title = this.txtTitle.Text;
            DateTime publishingDate = Convert.ToDateTime(this.dtpPublishingDate.Text);
            string writer = this.txtWriter.Text;
            string genre = this.cmbGenre.Text;
            int copies = Convert.ToInt32( this.txtCopies.Text);
            return new Book(id, title, publishingDate, writer, genre, copies);


        }
        internal Book GetBook()
        {
            DataTable dt = BookRepo.GetSelectedBooks(ID);
            string id = dt.Rows[0][0].ToString();
            string title = dt.Rows[0][1].ToString();
            DateTime publishingDate = Convert.ToDateTime(dt.Rows[0][2].ToString());
            string writer = dt.Rows[0][3].ToString();
            string genre = dt.Rows[0][4].ToString();
            int copies = Convert.ToInt32(dt.Rows[0][5].ToString());
            string availability = dt.Rows[0][6].ToString();
            return new Book(id, title, publishingDate, writer, genre, copies);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {   
            if(CheckFields())
            {
                BookRepo.UpdateBook(FieldToClass());
                this.Pass();
                this.Close();
            }
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
             this.Close();
        }
    }
}
