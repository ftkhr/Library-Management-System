﻿namespace Library_Management_System.LMS.AdminForms
{
    partial class FormAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAdmin));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnLogout = new System.Windows.Forms.Button();
            this.btnRequests = new System.Windows.Forms.Button();
            this.pnlSelect = new System.Windows.Forms.Panel();
            this.btnAdminInfo = new System.Windows.Forms.Button();
            this.btnMembers = new System.Windows.Forms.Button();
            this.btnBook = new System.Windows.Forms.Button();
            this.pnlBody = new System.Windows.Forms.Panel();
            this.ucMembers1 = new Library_Management_System.LMS.AdminForms.UCMembers();
            this.ucBook1 = new Library_Management_System.LMS.AdminForms.UCBook();
            this.ucApproval1 = new Library_Management_System.LMS.AdminForms.UCApproval();
            this.ucAdminInfo1 = new Library_Management_System.LMS.AdminForms.UCAdminInfo();
            this.panel1.SuspendLayout();
            this.pnlBody.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.panel1.Controls.Add(this.btnLogout);
            this.panel1.Controls.Add(this.btnRequests);
            this.panel1.Controls.Add(this.pnlSelect);
            this.panel1.Controls.Add(this.btnAdminInfo);
            this.panel1.Controls.Add(this.btnMembers);
            this.panel1.Controls.Add(this.btnBook);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(233, 561);
            this.panel1.TabIndex = 0;
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(21)))), ((int)(((byte)(64)))));
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.ForeColor = System.Drawing.Color.White;
            this.btnLogout.Image = ((System.Drawing.Image)(resources.GetObject("btnLogout.Image")));
            this.btnLogout.Location = new System.Drawing.Point(12, 476);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(73, 73);
            this.btnLogout.TabIndex = 4;
            this.btnLogout.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnLogout.UseVisualStyleBackColor = false;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // btnRequests
            // 
            this.btnRequests.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRequests.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRequests.ForeColor = System.Drawing.Color.White;
            this.btnRequests.Image = ((System.Drawing.Image)(resources.GetObject("btnRequests.Image")));
            this.btnRequests.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnRequests.Location = new System.Drawing.Point(9, 59);
            this.btnRequests.Name = "btnRequests";
            this.btnRequests.Size = new System.Drawing.Size(213, 86);
            this.btnRequests.TabIndex = 3;
            this.btnRequests.Text = "Requests";
            this.btnRequests.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnRequests.UseVisualStyleBackColor = true;
            this.btnRequests.Click += new System.EventHandler(this.btnRequests_Click);
            // 
            // pnlSelect
            // 
            this.pnlSelect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(177)))), ((int)(((byte)(44)))));
            this.pnlSelect.Location = new System.Drawing.Point(-3, 59);
            this.pnlSelect.Name = "pnlSelect";
            this.pnlSelect.Size = new System.Drawing.Size(15, 86);
            this.pnlSelect.TabIndex = 0;
            // 
            // btnAdminInfo
            // 
            this.btnAdminInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdminInfo.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdminInfo.ForeColor = System.Drawing.Color.White;
            this.btnAdminInfo.Image = ((System.Drawing.Image)(resources.GetObject("btnAdminInfo.Image")));
            this.btnAdminInfo.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAdminInfo.Location = new System.Drawing.Point(9, 335);
            this.btnAdminInfo.Name = "btnAdminInfo";
            this.btnAdminInfo.Size = new System.Drawing.Size(213, 86);
            this.btnAdminInfo.TabIndex = 2;
            this.btnAdminInfo.Text = "Admin Info";
            this.btnAdminInfo.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAdminInfo.UseVisualStyleBackColor = true;
            this.btnAdminInfo.Click += new System.EventHandler(this.btnAdminInfo_Click);
            // 
            // btnMembers
            // 
            this.btnMembers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMembers.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMembers.ForeColor = System.Drawing.Color.White;
            this.btnMembers.Image = ((System.Drawing.Image)(resources.GetObject("btnMembers.Image")));
            this.btnMembers.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnMembers.Location = new System.Drawing.Point(9, 243);
            this.btnMembers.Name = "btnMembers";
            this.btnMembers.Size = new System.Drawing.Size(213, 86);
            this.btnMembers.TabIndex = 1;
            this.btnMembers.Text = "Members";
            this.btnMembers.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnMembers.UseVisualStyleBackColor = true;
            this.btnMembers.Click += new System.EventHandler(this.btnMembers_Click);
            // 
            // btnBook
            // 
            this.btnBook.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBook.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBook.ForeColor = System.Drawing.Color.White;
            this.btnBook.Image = ((System.Drawing.Image)(resources.GetObject("btnBook.Image")));
            this.btnBook.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBook.Location = new System.Drawing.Point(9, 151);
            this.btnBook.Name = "btnBook";
            this.btnBook.Size = new System.Drawing.Size(213, 86);
            this.btnBook.TabIndex = 0;
            this.btnBook.Text = "Book";
            this.btnBook.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBook.UseVisualStyleBackColor = true;
            this.btnBook.Click += new System.EventHandler(this.btnBook_Click);
            // 
            // pnlBody
            // 
            this.pnlBody.AutoSize = true;
            this.pnlBody.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.pnlBody.Controls.Add(this.ucMembers1);
            this.pnlBody.Controls.Add(this.ucBook1);
            this.pnlBody.Controls.Add(this.ucApproval1);
            this.pnlBody.Controls.Add(this.ucAdminInfo1);
            this.pnlBody.Location = new System.Drawing.Point(228, 0);
            this.pnlBody.Name = "pnlBody";
            this.pnlBody.Size = new System.Drawing.Size(789, 956);
            this.pnlBody.TabIndex = 1;
            // 
            // ucMembers1
            // 
            this.ucMembers1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.ucMembers1.Font = new System.Drawing.Font("Century", 8.25F);
            this.ucMembers1.Location = new System.Drawing.Point(0, 0);
            this.ucMembers1.Name = "ucMembers1";
            this.ucMembers1.Pass = null;
            this.ucMembers1.Size = new System.Drawing.Size(776, 561);
            this.ucMembers1.TabIndex = 3;
            // 
            // ucBook1
            // 
            this.ucBook1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.ucBook1.Font = new System.Drawing.Font("Century", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucBook1.Location = new System.Drawing.Point(3, 0);
            this.ucBook1.Name = "ucBook1";
            this.ucBook1.Pass = null;
            this.ucBook1.Size = new System.Drawing.Size(776, 561);
            this.ucBook1.TabIndex = 2;
            // 
            // ucApproval1
            // 
            this.ucApproval1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.ucApproval1.Font = new System.Drawing.Font("Century", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucApproval1.ForeColor = System.Drawing.Color.Black;
            this.ucApproval1.Location = new System.Drawing.Point(0, -3);
            this.ucApproval1.Name = "ucApproval1";
            this.ucApproval1.Size = new System.Drawing.Size(776, 561);
            this.ucApproval1.TabIndex = 1;
            this.ucApproval1.Load += new System.EventHandler(this.ucApproval1_Load);
            // 
            // ucAdminInfo1
            // 
            this.ucAdminInfo1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.ucAdminInfo1.Font = new System.Drawing.Font("Century", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucAdminInfo1.Location = new System.Drawing.Point(0, 0);
            this.ucAdminInfo1.Name = "ucAdminInfo1";
            this.ucAdminInfo1.Size = new System.Drawing.Size(776, 561);
            this.ucAdminInfo1.TabIndex = 0;
            // 
            // FormAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 561);
            this.Controls.Add(this.pnlBody);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormAdmin";
            this.Text = "Admin Panel";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormAdmin_FormClosing);
            this.panel1.ResumeLayout(false);
            this.pnlBody.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnBook;
        private System.Windows.Forms.Panel pnlBody;
        private System.Windows.Forms.Panel pnlSelect;
        private System.Windows.Forms.Button btnAdminInfo;
        private System.Windows.Forms.Button btnMembers;
        private System.Windows.Forms.Button btnRequests;
        private UCMembers ucMembers1;
        private UCBook ucBook1;
        private UCApproval ucApproval1;
        private UCAdminInfo ucAdminInfo1;
        private System.Windows.Forms.Button btnLogout;
    }
}