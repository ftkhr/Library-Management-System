﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library_Management_System.LMS.Repository;

namespace Library_Management_System.LMS.AdminForms
{
    public partial class UCApproval : UserControl
    {
        public UCApproval()
        {
            InitializeComponent();
        }
        public void PopulateGridViewBorrow()
        {
            this.DGVBorrow.AutoGenerateColumns = false;
            this.DGVBorrow.DataSource = null;
            this.DGVBorrow.DataSource = BorrowBookRepo.GetBorrowRequestList();
        }

      

        private void ApproveBorrow()
        {
            string bookid = this.DGVBorrow.CurrentRow.Cells["bookid"].Value.ToString();
            string memberid = this.DGVBorrow.CurrentRow.Cells["memberid"].Value.ToString();
            BorrowBookRepo.ApproveBorrow(memberid, bookid);
            PopulateGridViewBorrow();
        }

       
        public void PopulateGridViewReturn()
        {
            this.DGVReturn.AutoGenerateColumns = false;
            this.DGVReturn.DataSource = null;
            this.DGVReturn.DataSource = BorrowBookRepo.GetReturnRequestList();
        }
        public void ApproveReturn()
        {
            string bookid = this.DGVReturn.CurrentRow.Cells["rbookid"].Value.ToString();
            string memberid = this.DGVReturn.CurrentRow.Cells["rmemberid"].Value.ToString();
            int copies = Convert.ToInt32(this.DGVReturn.CurrentRow.Cells["rcopies"].Value.ToString());
            BorrowBookRepo.ReturnBook(bookid, memberid, copies);
            PopulateGridViewReturn();
        }

        private void btnApproveReturn_Click(object sender, EventArgs e)
        {
            this.ApproveReturn();
        }

        private void btnApproveBorrow_Click(object sender, EventArgs e)
        {
            this.ApproveBorrow();
        }

        private void txtBorrow_TextChanged(object sender, EventArgs e)
        {
            this.DGVBorrow.AutoGenerateColumns = false;
            this.DGVBorrow.DataSource = null;
            this.DGVBorrow.DataSource = BorrowBookRepo.GetSelectedBorrowRequest(this.txtBorrow.Text);
        }

        private void txtReturn_TextChanged(object sender, EventArgs e)
        {
            this.DGVReturn.AutoGenerateColumns = false;
            this.DGVReturn.DataSource = null;
            this.DGVReturn.DataSource = BorrowBookRepo.GetSelectedReturnRequest(this.txtReturn.Text);
        }

        private void approveReturnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ApproveReturn();
        }

        private void approveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ApproveBorrow();
        }
    }
}
