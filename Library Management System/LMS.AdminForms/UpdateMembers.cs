﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library_Management_System.LMS.Entities;
using Library_Management_System.LMS.Repository;

namespace Library_Management_System.LMS.AdminForms
{
    public partial class UpdateMembers : Form
    {   
        PassMethod Pass { get; set; }
        internal string ID { get; set; }
        public UpdateMembers(PassMethod pass,string id)
        {
            InitializeComponent();
            this.Pass = pass;
            this.ID = id;
            this.FillInfo(GetMember());
        }
        internal void FillInfo(Member member)
        {
            this.txtAddress.Text = member.Adress;
            this.txtEmail.Text = member.Email;
            this.txtName.Text = member.Name;
            this.txtPassword.Text = member.Password;
            this.txtPhone.Text = member.Phone;
            this.dtpDob.Text = member.DOB.ToString();
            this.cmbAccountStatus.Text = member.AccountStatus;
            this.txtFine.Text = member.Fine.ToString();
            this.txtBooksBorrowed.Text= member.BorrowedBook.ToString();
        }
        internal Member FieldToClass()
        {
            string id = ID;
            string email=this.txtEmail.Text ; 
            string name= this.txtName.Text ;
            string password= this.txtPassword.Text ;
            string address = this.txtAddress.Text;
            string phone= this.txtPhone.Text ;
            DateTime dob= Convert.ToDateTime( this.dtpDob.Text );
            string accountStatus= this.cmbAccountStatus.Text ;
            int fine= Convert.ToInt32( this.txtFine.Text) ;
            int borrowedBook = Convert.ToInt32(this.txtBooksBorrowed.Text);
            return new Member(id, name, address, dob,phone, email, password, accountStatus, fine, borrowedBook);
        }
        internal Member GetMember()
        {
            DataTable dt = MemberRepo.GetSelectedMember(ID);
            string id = dt.Rows[0][0].ToString();
            string name = dt.Rows[0][1].ToString();
            string address = dt.Rows[0][2].ToString();
            DateTime dob = Convert.ToDateTime(dt.Rows[0][3].ToString());
            string phone = dt.Rows[0][4].ToString();
            string email = dt.Rows[0][5].ToString();
            string password = dt.Rows[0][6].ToString();
            string accountStatus = dt.Rows[0][7].ToString();
            int fine = Convert.ToInt32(dt.Rows[0][8].ToString()); 
            int borrowedBook = Convert.ToInt32(dt.Rows[0][9].ToString());
            return new Member(id, name, address, dob, phone, email, password, accountStatus, fine, borrowedBook);

        }
        
        public bool CheckFields()
        {
            if (this.txtAddress.Text == "" || this.txtEmail.Text == "" || this.txtFine.Text == "" || this.txtName.Text == "" || this.txtPassword.Text == "" || this.txtPhone.Text == "" || this.cmbAccountStatus.Text == "" || this.dtpDob.Text == "")
            {
                MessageBox.Show("Invalid or Empty info");
                return false;
            }
            else if (this.txtPhone.Text.Length != 11 || !int.TryParse(this.txtPhone.Text, out int i))
            {
                MessageBox.Show("Invalid Phone Number");
                return false;
            }
            else return true;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (CheckFields())
            {
                MemberRepo.UpdateMember(FieldToClass());
                this.Pass();
                this.Close();
            }
        }

       
    }
}
