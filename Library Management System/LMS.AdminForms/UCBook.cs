﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library_Management_System.LMS.Entities;
using Library_Management_System.LMS.Repository;

namespace Library_Management_System.LMS.AdminForms
{

    
    public partial class UCBook : UserControl
    {
        
        public PassMethod Pass { get; set; }
        
        public UCBook()
        {
            InitializeComponent();
            PopulateGirdView();
        }
        
        public void PopulateGirdView()
        {
            this.dgvBookList.AutoGenerateColumns = false;
            this.dgvBookList.DataSource = null;
            this.dgvBookList.DataSource = BookRepo.GetBookList();
        }
        public void CreateBook()
        {
            Book temp = new Book(this.txtTitle.Text, Convert.ToDateTime(this.dtpPublishingDate.Text), this.txtWriter.Text, this.cmbGenre.Text, Convert.ToInt32(this.txtCopies.Text));
            BookRepo.InsertBook(temp);
            PopulateGirdView();
        }
        public void ClearFields()
        {
            this.txtTitle.Clear();
            this.txtWriter.Clear();
            this.cmbGenre.Text = "";
            this.txtCopies.Clear();
        }
        public bool CheckFields()
        {
            if (txtCopies.Text == "" || txtTitle.Text == "" || txtWriter.Text == "" || cmbGenre.Text == "")
            {
                MessageBox.Show("Invalid Info");
                return false;
            }
            else if (!int.TryParse(txtCopies.Text.ToString(), out int i))
            {
                MessageBox.Show("Invalid Info");
                return false;
            }
            else return true;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            if(CheckFields())
            {
                CreateBook();
                ClearFields();
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            this.dgvBookList.AutoGenerateColumns = false;
            this.dgvBookList.DataSource = null;
            this.dgvBookList.DataSource = BookRepo.GetSelectedBooks(this.txtSearch.Text);
        }
        public void DeleteCommand()
        {
            BookRepo.DeleteBook(dgvBookList.CurrentRow.Cells["id"].Value.ToString());
            PopulateGirdView();
        }
       

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Pass = DeleteCommand;
            ConfirmationPanel cp = new ConfirmationPanel(Pass);
            cp.Show();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Pass = DeleteCommand;
            ConfirmationPanel cp = new ConfirmationPanel(Pass);
            cp.Show();

        }

        private void dgvBookList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            Pass = PopulateGirdView;
            //MessageBox.Show(dgvBookList.CurrentRow.Cells["id"].Value.ToString());
            FormUpdateBooks ub = new FormUpdateBooks(dgvBookList.CurrentRow.Cells["id"].Value.ToString(), Pass);
            ub.Show();
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Pass = PopulateGirdView;
            //MessageBox.Show(dgvBookList.CurrentRow.Cells["id"].Value.ToString());
            FormUpdateBooks ub = new FormUpdateBooks(dgvBookList.CurrentRow.Cells["id"].Value.ToString(), Pass);
            ub.Show();
        }

        
    }
}
