USE [master]
GO
/****** Object:  Database [LibraryManagemenSystem]    Script Date: 4/26/2019 11:32:34 PM ******/
CREATE DATABASE [LibraryManagemenSystem]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'LibraryManagemenSystem', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\LibraryManagemenSystem.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'LibraryManagemenSystem_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\LibraryManagemenSystem_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [LibraryManagemenSystem] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LibraryManagemenSystem].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LibraryManagemenSystem] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LibraryManagemenSystem] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LibraryManagemenSystem] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LibraryManagemenSystem] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LibraryManagemenSystem] SET ARITHABORT OFF 
GO
ALTER DATABASE [LibraryManagemenSystem] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LibraryManagemenSystem] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [LibraryManagemenSystem] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LibraryManagemenSystem] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LibraryManagemenSystem] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LibraryManagemenSystem] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LibraryManagemenSystem] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LibraryManagemenSystem] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LibraryManagemenSystem] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LibraryManagemenSystem] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LibraryManagemenSystem] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LibraryManagemenSystem] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LibraryManagemenSystem] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LibraryManagemenSystem] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LibraryManagemenSystem] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LibraryManagemenSystem] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LibraryManagemenSystem] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LibraryManagemenSystem] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LibraryManagemenSystem] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [LibraryManagemenSystem] SET  MULTI_USER 
GO
ALTER DATABASE [LibraryManagemenSystem] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LibraryManagemenSystem] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LibraryManagemenSystem] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LibraryManagemenSystem] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [LibraryManagemenSystem]
GO
/****** Object:  Table [dbo].[book]    Script Date: 4/26/2019 11:32:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[book](
	[id] [varchar](50) NOT NULL,
	[title] [varchar](50) NOT NULL,
	[publishingdate] [date] NOT NULL,
	[writer] [varchar](50) NOT NULL,
	[genre] [varchar](50) NOT NULL,
	[copies] [int] NOT NULL,
	[availability] [varchar](50) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[borrowbook]    Script Date: 4/26/2019 11:32:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[borrowbook](
	[bookid] [varchar](50) NOT NULL,
	[bookname] [varchar](50) NOT NULL,
	[copies] [int] NOT NULL,
	[memberid] [varchar](50) NOT NULL,
	[duration] [date] NOT NULL,
	[status] [varchar](50) NOT NULL,
	[returned] [varchar](50) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Librarian]    Script Date: 4/26/2019 11:32:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Librarian](
	[id] [varchar](50) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[address] [varchar](50) NOT NULL,
	[phone] [varchar](11) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[dob] [date] NOT NULL,
	[password] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Librarian] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[members]    Script Date: 4/26/2019 11:32:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[members](
	[id] [varchar](50) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[address] [varchar](100) NOT NULL,
	[dob] [date] NOT NULL,
	[phone] [varchar](50) NOT NULL,
	[email] [varchar](50) NOT NULL,
	[password] [varchar](50) NOT NULL,
	[accountstatus] [varchar](50) NOT NULL,
	[fine] [int] NOT NULL,
	[booksborrowed] [int] NULL,
 CONSTRAINT [PK_members] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[book] ([id], [title], [publishingdate], [writer], [genre], [copies], [availability]) VALUES (N'B-0', N'The Lord of the Rings', CAST(0x19E30A00 AS Date), N'J. R. R. Tolkien', N'Fantasy', 2, N'available')
INSERT [dbo].[book] ([id], [title], [publishingdate], [writer], [genre], [copies], [availability]) VALUES (N'B-1', N'A Song of Ice and Fire', CAST(0xF93E0B00 AS Date), N'George R. R. Martin', N'Fantasy', 2, N'available')
INSERT [dbo].[book] ([id], [title], [publishingdate], [writer], [genre], [copies], [availability]) VALUES (N'B-2', N'The Lion, The Witch and The Wardrobe', CAST(0x9F0D0B00 AS Date), N'C. S. Lewis', N'Fantasy', 3, N'available')
INSERT [dbo].[book] ([id], [title], [publishingdate], [writer], [genre], [copies], [availability]) VALUES (N'B-3', N'The Haunting of Hill House', CAST(0xF3E90A00 AS Date), N'Shirley Jackson', N'Horror', 1, N'available')
INSERT [dbo].[book] ([id], [title], [publishingdate], [writer], [genre], [copies], [availability]) VALUES (N'B-4', N'House of Leaves', CAST(0x4C240B00 AS Date), N'Mark Z. Danielewski', N'Horror', 4, N'available')
INSERT [dbo].[book] ([id], [title], [publishingdate], [writer], [genre], [copies], [availability]) VALUES (N'B-5', N'The Shining', CAST(0x37030B00 AS Date), N'Stephen King', N'Horror', 4, N'available')
INSERT [dbo].[book] ([id], [title], [publishingdate], [writer], [genre], [copies], [availability]) VALUES (N'B-6', N'1984', CAST(0xE2DB0A00 AS Date), N'George Orwell', N'Science Fiction', 6, N'available')
INSERT [dbo].[book] ([id], [title], [publishingdate], [writer], [genre], [copies], [availability]) VALUES (N'B-7', N'Brave New World', CAST(0xA3C30A00 AS Date), N'Aldous Huxley', N'Science Fiction', 4, N'available')
INSERT [dbo].[book] ([id], [title], [publishingdate], [writer], [genre], [copies], [availability]) VALUES (N'B-8', N'The Time Machine', CAST(0x408E0A00 AS Date), N'H. G. Wells', N'Science Fiction', 3, N'available')
INSERT [dbo].[book] ([id], [title], [publishingdate], [writer], [genre], [copies], [availability]) VALUES (N'B-9', N'Fifty Shades of Grey', CAST(0x31340B00 AS Date), N'E. L. James', N'Romance', 4, N'available')
INSERT [dbo].[book] ([id], [title], [publishingdate], [writer], [genre], [copies], [availability]) VALUES (N'B-10', N'Love Story', CAST(0xA1F90A00 AS Date), N'Erich Segal', N'Romance', 5, N'available')
INSERT [dbo].[book] ([id], [title], [publishingdate], [writer], [genre], [copies], [availability]) VALUES (N'B-11', N'Outlander', CAST(0xC7170B00 AS Date), N'Diana Gabaldon', N'Romance', 5, N'available')
INSERT [dbo].[book] ([id], [title], [publishingdate], [writer], [genre], [copies], [availability]) VALUES (N'B-12', N'The Silent Patient', CAST(0x463F0B00 AS Date), N'Alex Michaelides', N'Thriller', 5, N'available')
INSERT [dbo].[book] ([id], [title], [publishingdate], [writer], [genre], [copies], [availability]) VALUES (N'B-13', N'Memory Man', CAST(0xD6390B00 AS Date), N'David Baldacci', N'Thriller', 3, N'available')
INSERT [dbo].[book] ([id], [title], [publishingdate], [writer], [genre], [copies], [availability]) VALUES (N'B-14', N'The Wife Between Us', CAST(0xBE3D0B00 AS Date), N'Greer Hendricks', N'Thriller', 5, N'available')
INSERT [dbo].[book] ([id], [title], [publishingdate], [writer], [genre], [copies], [availability]) VALUES (N'B-15', N'the mysterious affair at styles', CAST(0xFDB20A00 AS Date), N'Agatha Christie', N'Detective', 5, N'available')
INSERT [dbo].[book] ([id], [title], [publishingdate], [writer], [genre], [copies], [availability]) VALUES (N'B-16', N'The Girl with the Dragon Tattoo', CAST(0x062C0B00 AS Date), N'Stieg Larsson', N'Detective', 4, N'available')
INSERT [dbo].[book] ([id], [title], [publishingdate], [writer], [genre], [copies], [availability]) VALUES (N'B-17', N'"A" is for Alibi', CAST(0xC10A0B00 AS Date), N'Sue Grafton', N'Detective', 4, N'available')
INSERT [dbo].[book] ([id], [title], [publishingdate], [writer], [genre], [copies], [availability]) VALUES (N'B-18', N'Dracula', CAST(0x94130B00 AS Date), N'Bram Stoker', N'Horror', 5, N'available')
INSERT [dbo].[borrowbook] ([bookid], [bookname], [copies], [memberid], [duration], [status], [returned]) VALUES (N'B-8', N'The Time Machine', 1, N'M-0', CAST(0x9C3F0B00 AS Date), N'unapproved', N'N/A')
INSERT [dbo].[borrowbook] ([bookid], [bookname], [copies], [memberid], [duration], [status], [returned]) VALUES (N'B-0', N'The Lord of the Rings', 3, N'M-1', CAST(0x9C3F0B00 AS Date), N'approved', N'requested')
INSERT [dbo].[borrowbook] ([bookid], [bookname], [copies], [memberid], [duration], [status], [returned]) VALUES (N'B-3', N'The Haunting of Hill House', 1, N'M-1', CAST(0x9C3F0B00 AS Date), N'unapproved', N'N/A')
INSERT [dbo].[borrowbook] ([bookid], [bookname], [copies], [memberid], [duration], [status], [returned]) VALUES (N'B-2', N'The Lion, The Witch and The Wardrobe', 2, N'M-0', CAST(0x9D3F0B00 AS Date), N'unapproved', N'N/A')
INSERT [dbo].[Librarian] ([id], [name], [address], [phone], [email], [dob], [password]) VALUES (N'L-0', N'Rahim Islam', N'177,Kuratoli,Kuril Bishwa Road', N'01713068254', N'rahim@aiub.edu', CAST(0xCD210B00 AS Date), N'@!@#')
INSERT [dbo].[members] ([id], [name], [address], [dob], [phone], [email], [password], [accountstatus], [fine], [booksborrowed]) VALUES (N'M-0', N'Mihal Azmaeen', N'370 East Goran , Khilgaon', CAST(0x87210B00 AS Date), N'01735687941', N'mihal@gmail.com', N'asd', N'valid', 0, 3)
INSERT [dbo].[members] ([id], [name], [address], [dob], [phone], [email], [password], [accountstatus], [fine], [booksborrowed]) VALUES (N'M-1', N'Iftekhar Alam Khan', N'177 West Shewrapara, Mirpur , Dhaka', CAST(0xCD210B00 AS Date), N'01621356750', N'iftekhar@gmail.com', N'@iftekhar', N'valid', 0, 4)
INSERT [dbo].[members] ([id], [name], [address], [dob], [phone], [email], [password], [accountstatus], [fine], [booksborrowed]) VALUES (N'M-2', N'Abul Kashem', N'144, kuratoli , kuril ', CAST(0xDE210B00 AS Date), N'01789654423', N'abul@gmail.com', N'@abul', N'valid', 0, 0)
INSERT [dbo].[members] ([id], [name], [address], [dob], [phone], [email], [password], [accountstatus], [fine], [booksborrowed]) VALUES (N'M-3', N'Ashfaq Afzal Chowdhury', N'Sattar Molla Road , Mirpur 12', CAST(0x631F0B00 AS Date), N'01795606454', N'aacfahim@gmail.com', N'@ashfaq', N'valid', 0, 0)
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_members_uniqueemail]    Script Date: 4/26/2019 11:32:34 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_members_uniqueemail] ON [dbo].[members]
(
	[email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_members_uniquephone]    Script Date: 4/26/2019 11:32:34 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_members_uniquephone] ON [dbo].[members]
(
	[phone] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [LibraryManagemenSystem] SET  READ_WRITE 
GO
