﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library_Management_System.LMS_Entities
{
    abstract class Person
    {
        protected static int count;
        protected string id;

        public string Name { get; set; }
        public string Adress { get; set; }
        public DateTime DOB { get; set; }
        public int Phone { get; set; }
        public string email { get; set; }
        

    }
}
