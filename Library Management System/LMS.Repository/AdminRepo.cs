﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library_Management_System.LMS.Entities;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Windows.Forms;
using System.Data;
using Library_Management_System.LMS.DataLayer;
namespace Library_Management_System.LMS.Repository
{
    public class AdminRepo
    {
        public Admin librarian = new Admin();
        
        public static DataTable GetAllAdmin()
        {
            string SQL = "select * from librarian ";
            return DataAccess.GetDataTable(SQL);
            
        }
        
       
        public static void UpdateAdminTable(Admin admin)
        {
           
            string SQL = @"update Librarian set id='" + admin.ID +
                @"',name='" + admin.Name + 
                @"',Address='" + admin.Adress + 
                @"',phone='" + admin.Phone + 
                @"',email='" + admin.Email +
                @"',dob='" + admin.DOB + 
                @"',password='" + admin.Password+"';";
            try
            {
                DataAccess.ExecuteQuery(SQL);
                MessageBox.Show("Info Updated");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
       
        
        public static Admin AdminEntityToClass()
        {
            DataTable dt = GetAllAdmin();
            string id = dt.Rows[0][0].ToString();
            string name  = dt.Rows[0][1].ToString();
            string address = dt.Rows[0][2].ToString();
            string phone =  dt.Rows[0][3].ToString();
            string email = dt.Rows[0][4].ToString();
            DateTime dob = Convert.ToDateTime( dt.Rows[0][5].ToString());
            string password = dt.Rows[0][6].ToString();
            Admin temp = new Admin(name,address,dob,phone, email, password);
            return temp;
        }
        
    }
}
