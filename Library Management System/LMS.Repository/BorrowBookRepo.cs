﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library_Management_System.LMS.Entities;
using Library_Management_System.LMS.DataLayer;
using System.Windows.Forms;
using System.Data;

namespace Library_Management_System.LMS.Repository
{
    class BorrowBookRepo
    {
        public static void InsertBorrowBook(List<Book> booklist, string memberid)
        {
            bool executedWithoutError = false;
            for (int i =0; i<booklist.Count;i++)
            {
                Book temp = booklist[i];

                string sql = string.Format(@"insert into borrowbook values('{0}','{1}',{2},'{3}','{4}','unapproved','N/A');", temp.ID,temp.Title,temp.Copies, memberid,DateTime.Now.AddDays(7).ToString());
                //MessageBox.Show(sql);
                try
                {
                    DataAccess.ExecuteQuery(sql);
                    MessageBox.Show(sql);
                    MemberRepo.UpdateBorrowedBookCount(memberid, booklist[i].Copies);
                    SetBook(booklist[i]);
                    executedWithoutError = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    executedWithoutError = false;
                }
                
                if (executedWithoutError)
                {
                    MessageBox.Show(booklist[i].Title+ " Borrowed");
                }
                
            }
            
        }
        public static void SetBook(Book book)
        {

            if (BookRepo.GetBookCopies(book.ID) > book.Copies)
            {
                book.Copies = BookRepo.GetBookCopies(book.ID) - book.Copies;
                BookRepo.UpdateBook(book);
            }
            else if (BookRepo.GetBookCopies(book.ID) == book.Copies)
            {
                Book temp = book;
                book.Copies = 0;
                book.Availability = "unavailable";
                BookRepo.UpdateBook(book);
            }
            else
            {
                MessageBox.Show(book.Title + " Unavailable or limit reached");
            }
        }
        public static void ClearBorrowBook(string memberid,string bookid)
        {
            string sql = "delete from borrowbook where memberid='" + memberid + "' and bookid='"+bookid+"' ;";
            try
            {
                DataAccess.ExecuteQuery(sql);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public static DataTable GetBorrowRequestList()
        {
            string sql= "select * from borrowbook where status='unapproved';";
            return DataAccess.GetDataTable(sql);
        }
        public static DataTable GetReturnRequestList()
        {
            string sql = "select * from borrowbook where returned ='requested';";
            return DataAccess.GetDataTable(sql);
        }
        public static DataTable GetSelectedBorrowRequest(string text)
        {
            string sql = "select * from borrowbook where (bookid like '%" + text + "%') or (bookname like '%" + text + "%') or (memberid like  '%" + text + "%') and (status like 'unapproved') ;";
            return DataAccess.GetDataTable(sql);
        }
        public static DataTable GetSelectedReturnRequest(string text)
        {
            string sql = "select * from borrowbook where (bookid like '%" + text + "%') or (bookname like '%" + text + "%') or (memberid like  '%" + text + "%') and (returned like 'requested') ;";
            return DataAccess.GetDataTable(sql);
        }
        public static DataTable GetBooks(string id)
        {
            string sql = "select * from borrowbook where memberid='" + id + "';";
            return DataAccess.GetDataTable(sql);
        }
        public static void ApproveBorrow(string memberid , string bookid)
        {
            string sql = "update borrowbook set status='approved' ,returned = 'Not Returned' where bookid='" + bookid + "' and memberid = '" + memberid + "';";
            //MessageBox.Show(sql);
            try
            {
                DataAccess.ExecuteQuery(sql);
                MessageBox.Show("Approved");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public static void ApproveReturn(string memberid,string bookid)
        {
            string sql = "update borrowbook set status='approved' ,returned = 'requested' where bookid='" + bookid + "' and memberid = '" + memberid + "';";
            //MessageBox.Show(sql);
            try
            {
                DataAccess.ExecuteQuery(sql);
                MessageBox.Show("Approved");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public static bool ReturnedOnTime(string memberid,string bookid)
        {
            string sql = "select duration from borrowbook where bookid='" + bookid + "' and memberid = '" + memberid + "';";
            //MessageBox.Show(sql);
            DateTime dueDate = Convert.ToDateTime( DataAccess.GetDataTable(sql).Rows[0][0].ToString());
            if (System.DateTime.Now.CompareTo(dueDate) < 0)
            {
                return true;
            }
            else { return false; }
        }

        public static void ReturnBook(string bookid , string memberid, int copies)
        {
            if(!ReturnedOnTime(memberid,bookid))
            {
                MemberRepo.Addfine(memberid, (copies * 50));
            }
            BookRepo.SetBookCopies(bookid, copies);
            MemberRepo.UpdateBorrowedBookCount(memberid, (copies * -1));
            ClearBorrowBook(memberid, bookid);
            
            
        }
    }
}
