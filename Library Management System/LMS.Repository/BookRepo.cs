﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library_Management_System.LMS.Entities;
using Library_Management_System.LMS.DataLayer;
using System.Windows.Forms;
using System.Data;
namespace Library_Management_System.LMS.Repository
{
    class BookRepo
    {
        
        public BookRepo()
        {
            Book.count = 0;
        }
        public static int GetBookCount()
        {
            
            string sql = "select * from book";
            DataTable dt = DataAccess.GetDataTable(sql);
            string id = dt.Rows[dt.Rows.Count - 1][0].ToString();
            string idNumber = new String(id.Where(Char.IsDigit).ToArray());
            //MessageBox.Show(id);
            //MessageBox.Show(idNumber);
            return Convert.ToInt32(idNumber);
            
        }
        public static DataTable GetBookList(string sql = "select * from book")
        {
            DataAccess da = new DataAccess();
            return DataAccess.GetDataTable(sql);
        }
        public static void InsertBook(Book book)
        {
            string sql = string.Format(@"insert into book values('{0}','{1}','{2}','{3}','{4}',{5},'{6}');", book.ID, book.Title, book.PublishingDate, book.Writer, book.Genre, book.Copies, book.Availability);
            try
            {
                DataAccess.ExecuteQuery(sql);

                MessageBox.Show("Book Created");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }
        public static DataTable GetSelectedBooks(string text)
        {
            string sql = "select * from book where (id like '%" + text + "%') or (title like '%" + text + "%') or (writer like  '%" + text + "%') or (genre like  '%" + text + "%') ;";
            return GetBookList(sql);
        }
        public static int GetBookCopies(string id)
        {
            return Convert.ToInt32(GetSelectedBooks(id).Rows[0]["copies"]);
        }
        public static void DeleteBook(string id)
        {
            string sql = "delete from book where id='" + id + "';";
            try
            {
                DataAccess.ExecuteQuery(sql);
            }
            catch(Exception ex )
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public static void SetBookCopies(string id,int copies)
        {
            string sql = @"update book set copies=" +(GetBookCopies(id)+ copies).ToString() + ",availability='available' where id ='" + id + "' ;";
            
            //MessageBox.Show(sql);
            try
            {
                DataAccess.ExecuteQuery(sql);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public static void UpdateBook(Book book)
        {
            string sql = @"update book set title='" + book.Title +
                @"',publishingdate='" + book.PublishingDate.ToString() +
                @"',writer='" + book.Writer +
                @"',copies=" + book.Copies +
                @",genre='"+book.Genre +
                @"',availability='" + book.Availability + "' where id ='"+book.ID+"' ;";
            //MessageBox.Show(sql);
            try
            {
                DataAccess.ExecuteQuery(sql);
                MessageBox.Show("Info Updated");

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }

}
