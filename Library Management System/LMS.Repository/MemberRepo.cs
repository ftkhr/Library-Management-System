﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using Library_Management_System.LMS.DataLayer;
using Library_Management_System.LMS.Entities;

namespace Library_Management_System.LMS.Repository
{
    class MemberRepo
    {
        public static DataTable GetMembers(string sql = "select * from members")
        {
            return DataAccess.GetDataTable(sql);
        }
        public static int GetMemberCount()
        {
            string sql = "select * from members";
            DataTable dt = DataAccess.GetDataTable(sql);
            string id = dt.Rows[dt.Rows.Count - 1][0].ToString();
            string idNumber = new String(id.Where(Char.IsDigit).ToArray());
            //MessageBox.Show(id);
            //MessageBox.Show(idNumber);
            return (Convert.ToInt32(idNumber)+1);
        }
        public static void InsertMember(Member member)
        {
            string sql = string.Format(@"insert into members values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}',{8},{9});",member.ID, member.Name,member.Adress,member.DOB.ToString(),member.Phone,member.Email,member.Password,member.AccountStatus,member.Fine,member.BorrowedBook);
            //MessageBox.Show(sql);
            try
            {
                DataAccess.ExecuteQuery(sql);
                
                MessageBox.Show("Member Created");
            }       
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }
        public static string GetAccountStatus(string id)
        {
            string sql = "select accountstatus from members where id ='" + id + "';";
            //MessageBox.Show(sql);
            return DataAccess.GetDataTable(sql).Rows[0][0].ToString();
            
        }
        public static int GetFine(string id)
        {
            string sql = "select fine from members where id ='" + id + "';";
            return Convert.ToInt32(DataAccess.GetDataTable(sql).Rows[0][0].ToString());
        }
        public static void Addfine(string id, int amount)
        {
            string sql = @"update members set " +
            @"fine=" + (GetFine(id) + amount).ToString() + ",accountstatus='invalid' where id ='" + id + "';";
           // MessageBox.Show(sql);
            try
            {
                DataAccess.ExecuteQuery(sql);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public static void UpdateBorrowedBookCount(string id,int count)
        {
            string sql = @"update members set " +
                @"booksborrowed=" +(GetBorrowedBookAmmount(id)+count).ToString() + " where id ='" + id + "';";
           // MessageBox.Show(sql);
            try
            {
                DataAccess.ExecuteQuery(sql);
                //MessageBox.Show("Info Updated");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public static DataTable GetSelectedMember(string text)
        {
            string sql = "select * from members where (id like '%" + text + "%') or (name like '%" + text + "%') or(phone like  '%" + text + "%');";
            return GetMembers(sql);

        }
        public static void DeleteMember(string id)
        {
            string sql = "delete from members where id='" + id + "';";
            try
            {
                DataAccess.ExecuteQuery(sql);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public static void UpdatePassword(string id , string password)
        {
            string SQL = @"update members set " +
               @"password='" + password +
               @"' where id ='" + id + "';";
            //MessageBox.Show(SQL);
            try
            {
                DataAccess.ExecuteQuery(SQL);
                MessageBox.Show("Info Updated");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public static int GetBorrowedBookAmmount(string id)
        {
            string sql = " select booksborrowed from members where id='" + id + "';";
            //MessageBox.Show(sql);
            return Convert.ToInt32( DataAccess.GetDataTable(sql).Rows[0]["booksborrowed"].ToString());
        }
        public static void UpdateMember(Member member)
        {
            string SQL = @"update members set " +
                @"name='" + member.Name +
                @"',Address='" + member.Adress +
                @"',phone='" + member.Phone +
                @"',email='" + member.Email +
                @"',dob='" + member.DOB +
                @"',password='" + member.Password +
                @"',accountstatus='" + member.AccountStatus+
                @"',fine="+member.Fine+
                @",booksborrowed="+member.BorrowedBook+ " where id ='"+member.ID+"';";
           // MessageBox.Show(SQL);
            try
            {
                DataAccess.ExecuteQuery(SQL);
                MessageBox.Show("Info Updated");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            
        }
    }
}
