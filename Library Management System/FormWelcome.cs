﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library_Management_System.LMS.AdminForms;
using Library_Management_System.LMS.DataLayer;
using Library_Management_System.LMS.Memebers.Forms;

namespace Library_Management_System
{
    public partial class WelcomeMenu : Form
    {
        bool showed;
        public WelcomeMenu()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            this.Visible=false;
            FormAdmin fAdmin = new FormAdmin(this);
            fAdmin.ShowDialog();
        }

        private void WelcomeMenu_Load(object sender, EventArgs e)
        {
            pnlWelcome.BackColor = Color.FromArgb(150, 0, 0, 0);
            
        }

        private void btnLogin_Click_1(object sender, EventArgs e)
        {

            if (txtUserID.Text == "" || txtPassword.Text == "")
            {
                MessageBox.Show("Invalid Username or Password");
                txtUserID.Text = "";
                txtPassword.Text = "";
            }
            else if (txtUserID.Text.StartsWith("L-"))
            {

                string sql = "select * from Librarian where id='" + this.txtUserID.Text + "';";
                DataTable dt = DataAccess.GetDataTable(sql);

                if (dt.Rows[0]["password"].ToString() == txtPassword.Text)
                {
                    MessageBox.Show("Welcome Librarian " + dt.Rows[0]["name"].ToString());
                    this.Visible = false;
                    FormAdmin fa = new FormAdmin(this);
                    fa.Visible=true;
                    txtUserID.Text = "";
                    txtPassword.Text = "";
                }
                else
                {
                    MessageBox.Show("Wrong Username or Password");
                    txtUserID.Text = "";
                    txtPassword.Text = "";
                }

            }
            else if (txtUserID.Text.StartsWith("M-"))
            {
                string sql = "select * from members where id='" + this.txtUserID.Text + "';";
                DataTable dt = DataAccess.GetDataTable(sql);

                if (dt.Rows[0]["password"].ToString() == txtPassword.Text)
                {
                    MessageBox.Show("Welcome Member " + dt.Rows[0]["name"].ToString());
                    this.Hide();
                    FormMember fm = new FormMember(this.txtUserID.Text,this);
                    fm.Show();
                    txtUserID.Text = "";
                    txtPassword.Text = "";
                }
                else
                {
                    MessageBox.Show("Wrong Username or Password");
                    txtUserID.Text = "";
                    txtPassword.Text = "";
                }
            }


        }

        private void btnShowPassword_Click(object sender, EventArgs e)
        {
            if (!showed)
            {
                txtPassword.PasswordChar = '\0';

                showed = true;
            }
            else
            {
                txtPassword.PasswordChar = '*';

                showed = false;
            }
        }

        private void WelcomeMenu_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
