﻿namespace Library_Management_System.LMS.Members.Forms
{
    partial class ViewBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewBook));
            this.pnlAddBook = new System.Windows.Forms.Panel();
            this.lblPublishingTime = new System.Windows.Forms.Label();
            this.txtCopies = new System.Windows.Forms.TextBox();
            this.lblCopies = new System.Windows.Forms.Label();
            this.lblGenre = new System.Windows.Forms.Label();
            this.txtWriter = new System.Windows.Forms.TextBox();
            this.lblWriter = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblbookInfo = new System.Windows.Forms.Label();
            this.txtGenre = new System.Windows.Forms.TextBox();
            this.txtPublishingDate = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.pnlAddBook.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlAddBook
            // 
            this.pnlAddBook.Controls.Add(this.btnAdd);
            this.pnlAddBook.Controls.Add(this.txtPublishingDate);
            this.pnlAddBook.Controls.Add(this.txtGenre);
            this.pnlAddBook.Controls.Add(this.lblPublishingTime);
            this.pnlAddBook.Controls.Add(this.txtCopies);
            this.pnlAddBook.Controls.Add(this.lblCopies);
            this.pnlAddBook.Controls.Add(this.lblGenre);
            this.pnlAddBook.Controls.Add(this.txtWriter);
            this.pnlAddBook.Controls.Add(this.lblWriter);
            this.pnlAddBook.Controls.Add(this.txtTitle);
            this.pnlAddBook.Controls.Add(this.lblTitle);
            this.pnlAddBook.Controls.Add(this.lblbookInfo);
            this.pnlAddBook.Location = new System.Drawing.Point(12, 11);
            this.pnlAddBook.Name = "pnlAddBook";
            this.pnlAddBook.Size = new System.Drawing.Size(443, 269);
            this.pnlAddBook.TabIndex = 2;
            // 
            // lblPublishingTime
            // 
            this.lblPublishingTime.AutoSize = true;
            this.lblPublishingTime.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPublishingTime.ForeColor = System.Drawing.Color.White;
            this.lblPublishingTime.Location = new System.Drawing.Point(33, 170);
            this.lblPublishingTime.Name = "lblPublishingTime";
            this.lblPublishingTime.Size = new System.Drawing.Size(105, 17);
            this.lblPublishingTime.TabIndex = 10;
            this.lblPublishingTime.Text = "Publishing Time";
            // 
            // txtCopies
            // 
            this.txtCopies.Enabled = false;
            this.txtCopies.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCopies.Location = new System.Drawing.Point(119, 140);
            this.txtCopies.Name = "txtCopies";
            this.txtCopies.Size = new System.Drawing.Size(293, 22);
            this.txtCopies.TabIndex = 4;
            // 
            // lblCopies
            // 
            this.lblCopies.AutoSize = true;
            this.lblCopies.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCopies.ForeColor = System.Drawing.Color.White;
            this.lblCopies.Location = new System.Drawing.Point(33, 143);
            this.lblCopies.Name = "lblCopies";
            this.lblCopies.Size = new System.Drawing.Size(53, 17);
            this.lblCopies.TabIndex = 7;
            this.lblCopies.Text = "Copies";
            // 
            // lblGenre
            // 
            this.lblGenre.AutoSize = true;
            this.lblGenre.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGenre.ForeColor = System.Drawing.Color.White;
            this.lblGenre.Location = new System.Drawing.Point(33, 113);
            this.lblGenre.Name = "lblGenre";
            this.lblGenre.Size = new System.Drawing.Size(47, 17);
            this.lblGenre.TabIndex = 5;
            this.lblGenre.Text = "Genre";
            // 
            // txtWriter
            // 
            this.txtWriter.Enabled = false;
            this.txtWriter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWriter.Location = new System.Drawing.Point(119, 82);
            this.txtWriter.Name = "txtWriter";
            this.txtWriter.Size = new System.Drawing.Size(293, 22);
            this.txtWriter.TabIndex = 2;
            // 
            // lblWriter
            // 
            this.lblWriter.AutoSize = true;
            this.lblWriter.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWriter.ForeColor = System.Drawing.Color.White;
            this.lblWriter.Location = new System.Drawing.Point(33, 85);
            this.lblWriter.Name = "lblWriter";
            this.lblWriter.Size = new System.Drawing.Size(45, 17);
            this.lblWriter.TabIndex = 3;
            this.lblWriter.Text = "Writer";
            // 
            // txtTitle
            // 
            this.txtTitle.Enabled = false;
            this.txtTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitle.Location = new System.Drawing.Point(119, 54);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(293, 22);
            this.txtTitle.TabIndex = 1;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(33, 57);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(68, 17);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Book Title";
            // 
            // lblbookInfo
            // 
            this.lblbookInfo.AutoSize = true;
            this.lblbookInfo.Font = new System.Drawing.Font("Cooper Std Black", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblbookInfo.ForeColor = System.Drawing.Color.White;
            this.lblbookInfo.Location = new System.Drawing.Point(128, 0);
            this.lblbookInfo.Name = "lblbookInfo";
            this.lblbookInfo.Size = new System.Drawing.Size(168, 35);
            this.lblbookInfo.TabIndex = 0;
            this.lblbookInfo.Text = "Book Info";
            // 
            // txtGenre
            // 
            this.txtGenre.Enabled = false;
            this.txtGenre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGenre.Location = new System.Drawing.Point(119, 112);
            this.txtGenre.Name = "txtGenre";
            this.txtGenre.Size = new System.Drawing.Size(293, 22);
            this.txtGenre.TabIndex = 11;
            // 
            // txtPublishingDate
            // 
            this.txtPublishingDate.Enabled = false;
            this.txtPublishingDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPublishingDate.Location = new System.Drawing.Point(144, 167);
            this.txtPublishingDate.Name = "txtPublishingDate";
            this.txtPublishingDate.Size = new System.Drawing.Size(268, 22);
            this.txtPublishingDate.TabIndex = 12;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(189)))), ((int)(((byte)(50)))));
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Location = new System.Drawing.Point(174, 213);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(96, 40);
            this.btnAdd.TabIndex = 22;
            this.btnAdd.Text = "Add to Cart";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // ViewBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.ClientSize = new System.Drawing.Size(467, 292);
            this.Controls.Add(this.pnlAddBook);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ViewBook";
            this.Text = "ViewBook";
            this.pnlAddBook.ResumeLayout(false);
            this.pnlAddBook.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlAddBook;
        private System.Windows.Forms.Label lblPublishingTime;
        private System.Windows.Forms.TextBox txtCopies;
        private System.Windows.Forms.Label lblCopies;
        private System.Windows.Forms.Label lblGenre;
        private System.Windows.Forms.TextBox txtWriter;
        private System.Windows.Forms.Label lblWriter;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblbookInfo;
        private System.Windows.Forms.TextBox txtPublishingDate;
        private System.Windows.Forms.TextBox txtGenre;
        private System.Windows.Forms.Button btnAdd;
    }
}