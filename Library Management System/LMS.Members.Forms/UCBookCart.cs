﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library_Management_System.LMS.Repository;
using Library_Management_System.LMS.Entities;

namespace Library_Management_System.LMS.Members.Forms
{
    public partial class UCBookCart : UserControl
    {
        public List<Book> BookList = new List<Book>();
        private int previousbook;
        private int cartbooks=0;
        private int booksBorrowed=0;
        public string ID { get; set; }
        public UCBookCart()
        {
            InitializeComponent();
        }
        public void PopulateGridView()
        {
            previousbook = MemberRepo.GetBorrowedBookAmmount(ID);
            this.dgvCart.DataSource = null;
            this.dgvCart.AutoGenerateColumns = false;
            this.dgvCart.DataSource = BookList;
            this.lblPreviousBook.Text = "Previous Books: " + previousbook.ToString();
            this.lblCartBooks.Text = "Books on Cart: " + cartbooks.ToString() ;
        }
        public void AddBook(Book book, int quantity)
        {
            if ((previousbook + cartbooks + booksBorrowed+ quantity)> 5)
            {
                MessageBox.Show("Book Limit = 5 Reached\n Can't add to cart");
            }
            else
            {
                bool similarbook = false;
                
                for (int i = 0; i < BookList.Count; i++)
                {
                    if (BookList[i].ID == book.ID)
                    {
                        if (BookRepo.GetBookCopies(book.ID) >= (BookList[i].Copies + quantity))
                        {
                            BookList[i].Copies += quantity;
                            cartbooks += quantity;
                            this.PopulateGridView();
                            similarbook = true;
                            MessageBox.Show("Added to Cart");
                            
                        }
                        else
                        {
                            MessageBox.Show("Sorry Limit reached for " + BookList[i].Title);
                            similarbook = true;
                        }
                    }
                }
                if (!similarbook)
                {
                    book.Copies = quantity;
                    cartbooks += book.Copies;
                    this.BookList.Add(book);
                    this.PopulateGridView();
                    MessageBox.Show("Added to Cart");
                }
            }
        }
        public void RemoveBooks()
        {
            string id = this.dgvCart.CurrentRow.Cells["id"].Value.ToString();
            for(int i = 0;i<BookList.Count;i++)
            {
                if(BookList[i].ID==id)
                {
                    cartbooks -= BookList[i].Copies;
                    BookList.RemoveAt(i);
                }
            }
            this.PopulateGridView();
        }
        public void ClearCart()
        {
            this.BookList.Clear();
            this.cartbooks = 0;
            this.PopulateGridView();
        }
        private void btnRemove_Click(object sender, EventArgs e)
        {
            this.RemoveBooks();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.ClearCart();
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            if(BookList.Count>0)
            {
                BorrowBookRepo.InsertBorrowBook(BookList, ID);
                this.ClearCart();
            }
        }

        private void removeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.RemoveBooks();
        }
    }
}
