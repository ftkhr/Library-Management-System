﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library_Management_System.LMS.Repository;
using Library_Management_System.LMS.Entities;
using Library_Management_System.LMS.Memebers.Forms;

namespace Library_Management_System.LMS.Members.Forms
{
    public delegate void PassBook();
    public partial class UCBookList : UserControl
    {
        public string ID { get; set; }
        public PassMethod Pass { get; set; }
        public PassBook PB { get; set; }
        public UCBookList()
        {
            InitializeComponent();
            PopulateGirdView();
        }
        
        public void PopulateGirdView()
        {
            this.dgvBookList.AutoGenerateColumns = false;
            this.dgvBookList.DataSource = null;
            this.dgvBookList.DataSource = BookRepo.GetBookList();
        }

        private void txtSearch_TextChanged_1(object sender, EventArgs e)
        {
            this.dgvBookList.AutoGenerateColumns = false;
            this.dgvBookList.DataSource = null;
            this.dgvBookList.DataSource = BookRepo.GetSelectedBooks(this.txtSearch.Text);

        }
        private Book CreateBook()
        {
            DataTable dt = BookRepo.GetSelectedBooks(dgvBookList.CurrentRow.Cells["id"].Value.ToString());
            string id = dt.Rows[0][0].ToString();
            string title = dt.Rows[0][1].ToString();
            DateTime publish = Convert.ToDateTime(dt.Rows[0][2].ToString());
            string writer = dt.Rows[0][3].ToString(); 
            string genre = dt.Rows[0][4].ToString();
            int copies = Convert.ToInt32( dt.Rows[0][5].ToString()); 
            string available = dt.Rows[0][6].ToString();
            return new Book(id, title, publish, writer, genre, copies);
        }
        private void AddtoCart()
        {   

            if (MemberRepo.GetBorrowedBookAmmount(ID) > 5)
            {
                MessageBox.Show("Maximum amount of borrowing book reached\nYou can't borrow anymore books");
            }
            else if(MemberRepo.GetAccountStatus(ID)!="valid")
            {
                MessageBox.Show("Your account is not valid\nContactLibrarian");
            }
            else if (dgvBookList.CurrentRow.Cells["availability"].Value.ToString()=="unavailable")
            {
                MessageBox.Show("Sorry! Book not available");
            }
            else
            {
                Quantity quantity = new Quantity(MemberRepo.GetBorrowedBookAmmount(ID), this.CreateBook(), ID, Pass);
                quantity.Show();
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.AddtoCart();
        }
        private void ViewBook()
        {
            PB = this.AddtoCart;
            ViewBook VB = new ViewBook(CreateBook(), PB);
            VB.Show();
        }
       
        private void addToCartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.AddtoCart();
        }

        private void dgvBookList_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            this.ViewBook();
        }

        private void viewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ViewBook();
        }
    }
}
