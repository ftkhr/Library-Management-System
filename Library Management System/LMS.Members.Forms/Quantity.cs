﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library_Management_System.LMS.Repository;
using Library_Management_System.LMS.Entities;
using Library_Management_System.LMS.Memebers.Forms;

namespace Library_Management_System.LMS.Members.Forms
{
    public partial class Quantity : Form
    {   
        public PassMethod Pass { get; set; }
        public int BorrowedBook { get; set; }
        public Book book { get; set; }
        
        public string ID { get; set; }
        public Quantity(int borrowedBook, Book book, string id,PassMethod pass)
        {
            InitializeComponent();
            this.BorrowedBook =borrowedBook;
            this.book = book;
            this.ID = id;
            this.Pass = pass;
            this.txtCount.Text ="1";
        }

        private void btnPlus_Click(object sender, EventArgs e)
        {
            if((Convert.ToInt16(this.txtCount.Text)+1+BorrowedBook)>5|| (Convert.ToInt16(this.txtCount.Text) + 1)>book.Copies)
            {   

                MessageBox.Show((Convert.ToInt16(this.txtCount.Text)+1)+" Limit Reached");
            }
            else
            {
                this.txtCount.Text = (Convert.ToInt16(this.txtCount.Text) + 1).ToString();
            }
        }

        private void btnMinus_Click(object sender, EventArgs e)
        {
            if ((Convert.ToInt16(this.txtCount.Text) - 1) < 0)
            {
                
            }
            else
            {
                this.txtCount.Text = (Convert.ToInt16(this.txtCount.Text) - 1).ToString();
            }

        }

        private void btnAddToCart_Click(object sender, EventArgs e)
        {
            book.Copies = Convert.ToInt32(this.txtCount.Text);
            this.Pass(book, Convert.ToInt32(this.txtCount.Text));
            this.Close();
            /*int count = Convert.ToInt32(this.txtCount.Text);
            MessageBox.Show(count.ToString());
            for(int i=0;i< count ;i++)
            {
                book.Copies = 1;
                this.Pass(book, 1);
                this.Close();
            }*/
        }
    }
}
