﻿namespace Library_Management_System.LMS.Memebers.Forms
{
    partial class FormMember
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMember));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnLogout = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.btnCart = new System.Windows.Forms.Button();
            this.pnlSelect = new System.Windows.Forms.Panel();
            this.btnUserInfo = new System.Windows.Forms.Button();
            this.btnBookList = new System.Windows.Forms.Button();
            this.ucBookList1 = new Library_Management_System.LMS.Members.Forms.UCBookList();
            this.ucUserInfo1 = new Library_Management_System.LMS.Members.Forms.UCUserInfo();
            this.ucBookCart1 = new Library_Management_System.LMS.Members.Forms.UCBookCart();
            this.ucHome1 = new Library_Management_System.LMS.Members.Forms.UCHome();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(62)))), ((int)(((byte)(80)))));
            this.panel1.Controls.Add(this.btnLogout);
            this.panel1.Controls.Add(this.btnHome);
            this.panel1.Controls.Add(this.btnCart);
            this.panel1.Controls.Add(this.pnlSelect);
            this.panel1.Controls.Add(this.btnUserInfo);
            this.panel1.Controls.Add(this.btnBookList);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(226, 561);
            this.panel1.TabIndex = 1;
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(21)))), ((int)(((byte)(64)))));
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.ForeColor = System.Drawing.Color.White;
            this.btnLogout.Image = ((System.Drawing.Image)(resources.GetObject("btnLogout.Image")));
            this.btnLogout.Location = new System.Drawing.Point(12, 476);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(73, 73);
            this.btnLogout.TabIndex = 5;
            this.btnLogout.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnLogout.UseVisualStyleBackColor = false;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // btnHome
            // 
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.ForeColor = System.Drawing.Color.White;
            this.btnHome.Image = ((System.Drawing.Image)(resources.GetObject("btnHome.Image")));
            this.btnHome.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnHome.Location = new System.Drawing.Point(12, 59);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(213, 86);
            this.btnHome.TabIndex = 4;
            this.btnHome.Text = "Home";
            this.btnHome.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnHome.UseVisualStyleBackColor = true;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // btnCart
            // 
            this.btnCart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCart.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCart.ForeColor = System.Drawing.Color.White;
            this.btnCart.Image = ((System.Drawing.Image)(resources.GetObject("btnCart.Image")));
            this.btnCart.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCart.Location = new System.Drawing.Point(12, 243);
            this.btnCart.Name = "btnCart";
            this.btnCart.Size = new System.Drawing.Size(213, 86);
            this.btnCart.TabIndex = 3;
            this.btnCart.Text = "Cart";
            this.btnCart.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnCart.UseVisualStyleBackColor = true;
            this.btnCart.Click += new System.EventHandler(this.btnCart_Click);
            // 
            // pnlSelect
            // 
            this.pnlSelect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(177)))), ((int)(((byte)(44)))));
            this.pnlSelect.Location = new System.Drawing.Point(0, 59);
            this.pnlSelect.Name = "pnlSelect";
            this.pnlSelect.Size = new System.Drawing.Size(15, 86);
            this.pnlSelect.TabIndex = 0;
            // 
            // btnUserInfo
            // 
            this.btnUserInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUserInfo.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUserInfo.ForeColor = System.Drawing.Color.White;
            this.btnUserInfo.Image = ((System.Drawing.Image)(resources.GetObject("btnUserInfo.Image")));
            this.btnUserInfo.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnUserInfo.Location = new System.Drawing.Point(12, 335);
            this.btnUserInfo.Name = "btnUserInfo";
            this.btnUserInfo.Size = new System.Drawing.Size(213, 86);
            this.btnUserInfo.TabIndex = 2;
            this.btnUserInfo.Text = "UserInfo";
            this.btnUserInfo.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnUserInfo.UseVisualStyleBackColor = true;
            this.btnUserInfo.Click += new System.EventHandler(this.btnUserInfo_Click);
            // 
            // btnBookList
            // 
            this.btnBookList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBookList.Font = new System.Drawing.Font("Century Gothic", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBookList.ForeColor = System.Drawing.Color.White;
            this.btnBookList.Image = ((System.Drawing.Image)(resources.GetObject("btnBookList.Image")));
            this.btnBookList.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBookList.Location = new System.Drawing.Point(12, 151);
            this.btnBookList.Name = "btnBookList";
            this.btnBookList.Size = new System.Drawing.Size(213, 86);
            this.btnBookList.TabIndex = 0;
            this.btnBookList.Text = "Book List";
            this.btnBookList.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBookList.UseVisualStyleBackColor = true;
            this.btnBookList.Click += new System.EventHandler(this.btnBookList_Click);
            // 
            // ucBookList1
            // 
            this.ucBookList1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.ucBookList1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucBookList1.ForeColor = System.Drawing.Color.Black;
            this.ucBookList1.ID = null;
            this.ucBookList1.Location = new System.Drawing.Point(231, 0);
            this.ucBookList1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ucBookList1.Name = "ucBookList1";
            this.ucBookList1.Pass = null;
            this.ucBookList1.PB = null;
            this.ucBookList1.Size = new System.Drawing.Size(776, 561);
            this.ucBookList1.TabIndex = 3;
            // 
            // ucUserInfo1
            // 
            this.ucUserInfo1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.ucUserInfo1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucUserInfo1.ID = null;
            this.ucUserInfo1.Location = new System.Drawing.Point(231, 0);
            this.ucUserInfo1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ucUserInfo1.Name = "ucUserInfo1";
            this.ucUserInfo1.Size = new System.Drawing.Size(773, 561);
            this.ucUserInfo1.TabIndex = 4;
            // 
            // ucBookCart1
            // 
            this.ucBookCart1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.ucBookCart1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucBookCart1.ForeColor = System.Drawing.Color.Black;
            this.ucBookCart1.ID = null;
            this.ucBookCart1.Location = new System.Drawing.Point(231, 0);
            this.ucBookCart1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ucBookCart1.Name = "ucBookCart1";
            this.ucBookCart1.Size = new System.Drawing.Size(773, 555);
            this.ucBookCart1.TabIndex = 5;
            // 
            // ucHome1
            // 
            this.ucHome1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.ucHome1.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ucHome1.ForeColor = System.Drawing.Color.Black;
            this.ucHome1.Location = new System.Drawing.Point(231, 0);
            this.ucHome1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ucHome1.Name = "ucHome1";
            this.ucHome1.Size = new System.Drawing.Size(773, 555);
            this.ucHome1.TabIndex = 6;
            // 
            // FormMember
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(73)))), ((int)(((byte)(94)))));
            this.ClientSize = new System.Drawing.Size(1008, 561);
            this.Controls.Add(this.ucHome1);
            this.Controls.Add(this.ucBookCart1);
            this.Controls.Add(this.ucUserInfo1);
            this.Controls.Add(this.ucBookList1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Century Gothic", 8.25F);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormMember";
            this.Text = "User Panel";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMember_FormClosing);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlSelect;
        private System.Windows.Forms.Button btnUserInfo;
        private System.Windows.Forms.Button btnBookList;
        private System.Windows.Forms.Button btnCart;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Button btnLogout;
        private Members.Forms.UCBookList ucBookList1;
        private Members.Forms.UCUserInfo ucUserInfo1;
        private Members.Forms.UCBookCart ucBookCart1;
        private Members.Forms.UCHome ucHome1;
    }
}