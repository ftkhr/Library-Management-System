﻿using Library_Management_System.LMS.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library_Management_System;

namespace Library_Management_System.LMS.Memebers.Forms
{
    public delegate void PassMethod(Book book , int queantity);
    public partial class FormMember : Form
    {   
        public PassMethod Pass { get; set; }
        public Form WM { get; set; }
        public string ID
        {
            get;set;
        }
        public FormMember(string id,Form wm)
        {
            InitializeComponent();
            this.WM = wm;
            this.ID = id;
            this.ucUserInfo1.ID = this.ID;
            this.ucBookList1.ID = this.ID;
            //this.ucCart1.ID = this.ID;
            //this.ucCart1.SetPreviousCount();
            this.ucUserInfo1.FillInfo(id);
            this.ucHome1.ID = ID;
            this.ucHome1.PopulateGridView();
            this.ucHome1.BringToFront();
            this.ucBookCart1.ID = ID;
            
        }
        private void Logout()
        {
            //WelcomeMenu WM = new WelcomeMenu();
            WM.Visible = true;
            this.Visible = false;
        }

        private void btnBookList_Click(object sender, EventArgs e)
        {
            pnlSelect.Height = btnBookList.Height;
            pnlSelect.Top = btnBookList.Top;
            this.ucBookList1.BringToFront();
            this.ucBookList1.PopulateGirdView();
            //this.Pass = ucCart1.AddBooks;
            this.Pass = ucBookCart1.AddBook;
            this.ucBookList1.Pass = Pass;
            
        }

        private void btnUserInfo_Click(object sender, EventArgs e)
        {
            pnlSelect.Height = btnUserInfo.Height;
            pnlSelect.Top = btnUserInfo.Top;
            this.ucUserInfo1.BringToFront();
            
        }

        private void FormMember_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Logout();
        }

        private void btnCart_Click(object sender, EventArgs e)
        {
            pnlSelect.Height = btnCart.Height;
            pnlSelect.Top = btnCart.Top;
            //ucCart1.BringToFront();
            this.ucBookCart1.BringToFront();
            this.ucBookCart1.PopulateGridView();
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            pnlSelect.Height = btnHome.Height;
            pnlSelect.Top = btnHome.Top;
            ucHome1.BringToFront();
            ucHome1.PopulateGridView();
        }
        
        private void btnLogout_Click(object sender, EventArgs e)
        {
            this.Logout();
        }
    }
}
