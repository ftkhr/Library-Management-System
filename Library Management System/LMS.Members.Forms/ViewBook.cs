﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library_Management_System.LMS.Entities;

namespace Library_Management_System.LMS.Members.Forms
{
    public partial class ViewBook : Form
    {   
        private Book Book { get; set; }
        private PassBook PB { get; set; }
        public ViewBook(Book book,PassBook pb)
        {   
            InitializeComponent();
            this.Book = book;
            this.PB = pb;
            this.FillInfo();
        }
        private void FillInfo()
        {
            this.txtCopies.Text = this.Book.Copies.ToString();
            this.txtGenre.Text = this.Book.Genre;
            this.txtPublishingDate.Text = this.Book.PublishingDate.ToString();
            this.txtTitle.Text = this.Book.Title;
            this.txtWriter.Text = this.Book.Writer;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            this.PB();
            this.Close();
        }
    }
}
