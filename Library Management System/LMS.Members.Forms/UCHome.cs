﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library_Management_System.LMS.Repository;

namespace Library_Management_System.LMS.Members.Forms
{
    public partial class UCHome : UserControl
    {
        public string ID;
        public UCHome()
        {
            InitializeComponent();
            
        }
        public void PopulateGridView()
        {
            this.dgvBorrowedBookList.DataSource = null;
            this.dgvBorrowedBookList.AutoGenerateColumns = false;
            this.dgvBorrowedBookList.DataSource = BorrowBookRepo.GetBooks(ID);
            this.lblAccountStatus.Text ="Account Status: " + MemberRepo.GetAccountStatus(ID);
            this.lblFine.Text = "Fine: " + MemberRepo.GetFine(ID).ToString();
            this.lblBooksBorrowed.Text = "Books Borrowed: "+MemberRepo.GetBorrowedBookAmmount(ID).ToString();
        }
        public void ReturnRequest()
        {
            if (this.dgvBorrowedBookList.CurrentRow.Cells["status"].Value.ToString() == "approved")
            {
                string bookid = this.dgvBorrowedBookList.CurrentRow.Cells["bookid"].Value.ToString();
                string memberid = this.dgvBorrowedBookList.CurrentRow.Cells["memberid"].Value.ToString();
                int copies = Convert.ToInt32(this.dgvBorrowedBookList.CurrentRow.Cells["copies"].Value.ToString());
                BorrowBookRepo.ApproveReturn(memberid, bookid);
            }
            else
            {
                MessageBox.Show("Book is not approved\n Contact Librarian");
            }
        }
       
        private void btnReturn_Click(object sender, EventArgs e)
        {
            this.ReturnRequest();
            PopulateGridView();
        }

        private void returnBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ReturnRequest();
            PopulateGridView();
        }
    }
}
