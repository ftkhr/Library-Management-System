﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library_Management_System.LMS.Repository;
namespace Library_Management_System.LMS.Members.Forms
{
    public partial class UCUserInfo : UserControl
    {
        public string ID { get;set; }
        public UCUserInfo()
        {
            InitializeComponent();
            //this.FillInfo();
        }

        public void FillInfo(string id)
        {
             DataTable dt= MemberRepo.GetSelectedMember(id);
            this.txtID.Text = dt.Rows[0][0].ToString();
            this.txtName.Text = dt.Rows[0][1].ToString();
            this.txtAddress.Text = dt.Rows[0][2].ToString();
            this.txtDOB.Text = dt.Rows[0][3].ToString();
            this.txtPhone.Text = dt.Rows[0][4].ToString();
            this.txtEmail.Text = dt.Rows[0][5].ToString();
            this.txtPassword.Text = dt.Rows[0][6].ToString();
            this.txtAccountStatus.Text = dt.Rows[0][7].ToString();
            this.txtFine.Text = dt.Rows[0][8].ToString();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (this.txtPassword.Text == "")
            {
                MessageBox.Show("Invalid Password");
                this.txtConfirmPassword.Clear();
            }
            else if(this.txtPassword.Text == this.txtConfirmPassword.Text)
            {
                MemberRepo.UpdatePassword(ID, this.txtPassword.Text);
                this.txtConfirmPassword.Clear();
            }
            else
            {
                MessageBox.Show("Passwords dont match");
                this.txtConfirmPassword.Clear();
            }

        }
    }
}
