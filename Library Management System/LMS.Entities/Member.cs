﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library_Management_System.LMS.Repository;
namespace Library_Management_System.LMS.Entities
{
    public class Member:Person
    {
        public string AccountStatus { get; set; }  
        public int Fine { get; set; }
        public int BorrowedBook { get; set; }
        public Member(string id , string name,string address , DateTime dob , string phone ,string email , string password,string accountstatus , int fine,int borrowedbook)
        {
            this.ID = id;
            this.Name = name;
            this.Adress = address;
            this.DOB = dob;
            this.Phone = phone;
            this.Email = email;
            this.Password = password;
            this.AccountStatus = accountstatus;
            this.Fine = fine;
            this.BorrowedBook = borrowedbook;
        }
        public Member(string name, string address, DateTime dob, string phone, string email, string password, string accountstatus, int fine, int borrowedbook)
        {
            this.ID = "M-"+MemberRepo.GetMemberCount();
            this.Name = name;
            this.Adress = address;
            this.DOB = dob;
            this.Phone = phone;
            this.Email = email;
            this.Password = password;
            this.AccountStatus = accountstatus;
            this.Fine = fine;
            this.BorrowedBook = borrowedbook;
        }
    }
}
