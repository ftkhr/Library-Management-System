﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library_Management_System.LMS.Repository;

namespace Library_Management_System.LMS.Entities
{
    public class Book
    {
        public static int count = 0;
        //private string id;
        public string ID
        {
            get;set;
        }
        public string Title { get; set; }
        public DateTime PublishingDate { get; set; }
        public string Writer { get; set; }
        public string Genre { get; set; }
        public int Copies { get; set; }
        public int BorrowBookID { get; set; }
        
        public string Availability { get; set; }

        public Book(string title,DateTime publish,string writer,string genre , int copies)
        {
            this.ID = "B-" + (BookRepo.GetBookCount()+1) ;
            this.Title = title;
            this.PublishingDate = publish;
            this.Writer = writer;
            this.Genre = genre;
            this.Copies = copies;
            if (copies >= 0)
            {
                this.Availability = "available";
            }
            else
            {
                this.Availability = "unavailable";
            }
        }
        public Book() { }
        public Book(string id, string title, DateTime publish, string writer, string genre, int copies)
        {
            this.ID = id;
            this.Title = title;
            this.PublishingDate = publish;
            this.Writer = writer;
            this.Genre = genre;
            this.Copies = copies;
            if (copies >= 0)
            {
                this.Availability = "available";
            }
            else
            {
                this.Availability = "unavailable";
            }
        }

    }
}
