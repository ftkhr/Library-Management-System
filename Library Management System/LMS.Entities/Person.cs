﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library_Management_System.LMS.Entities
{
    public abstract class Person
    {
        protected static int count;
        protected string id;
        public virtual string ID { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
        public DateTime DOB { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
